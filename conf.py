"""
Configuration variables
"""

# WLAN variables
dhcp = True
static_ip = None
netmask = None
gateway = None
dns = gateway
country = 'XX'
hostname = 'pymonitair'
ssid = 'SSID'
wlan_password = 'WLAN PASSWORD'

# Air quality data variables
json_url = 'URI to your JSON data file'
refresh_rate = 'time in seconds at which the sensors gets a new value'
pm2_5_critical = "critical threshold for the pm2.5 alerting. Set to None if you don't want alerts"

# Display variables
contrast = 128  # value from 0 to 255
timeout = 'value in multiples of refresh_rate after which the screen turns off'

# Debug
debug = False  # set to True to log all errors in /logs.txt
