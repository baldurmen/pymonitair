"""
Main function wrapper
"""

import os

import conf

from networking import connect
from frontends.ws_oled1_3 import init_oled
from loops import NestedLoops


def log_debug(value):
    """Duplicate stdout and stderr in a log file. Otherwise restart after crash"""
    if value is True:
        logfile = open('log.txt', 'a')
        os.dupterm(logfile)


def main():
    """Main function"""
    log_debug(conf.debug)
    connect()
    display, key0, key1 = init_oled()
    display.contrast(conf.contrast)
    loop = NestedLoops(display, key0, key1)
    loop.main_loop()


if __name__ == "__main__":
    main()
