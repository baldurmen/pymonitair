"""
Manage networking tasks
"""

import network
import time
import urequests

import conf

from frontends.ws_oled1_3 import print_error


def connect():
    """Connect to WLAN"""
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    if conf.dhcp is False:
        wlan.ifconfig((conf.static_ip, conf.netmask, conf.gateway, conf.dns))
    wlan.connect(conf.ssid, conf.wlan_password)
    while wlan.isconnected() is False:
        time.sleep(1)


def fetch_data(display):
    """Fetch JSON data from URL"""
    json_data = {}
    try:
        json_data = urequests.get(conf.json_url, timeout=4)
    except:  # Gracefully handle station not being online
        print_error(display)
        time.sleep(1)
    return json_data
