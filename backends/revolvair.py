"""
JSON parsing backing for the RevolvAir device
"""

import json

from urequests import Response


def json_to_dict(json_data):
    """Process JSON data from Revolvair device and output a dict"""
    processed_data = {'pm1': 'NA',
                      'pm2_5': 'NA',
                      'pm10': 'NA',
                      'temperature': 'NA',
                      'pressure': 'NA',
                      'humidity': 'NA'}
    if isinstance(json_data, Response):
        json_obj = json.loads(json_data.text)
        for value in json_obj['sensordatavalues']:
            if value['value_type'] == 'PMS_P0':
                processed_data['pm1'] = value['value']
            elif value['value_type'] == 'PMS_P2':
                processed_data['pm2_5'] = value['value']
            elif value['value_type'] == 'PMS_P1':
                processed_data['pm10'] = value['value']
            elif value['value_type'] == 'BME280_temperature':
                processed_data['temperature'] = value['value']
            elif value['value_type'] == 'BME280_pressure':
                processed_data['pressure'] = value['value']
            elif value['value_type'] == 'BME280_humidity':
                processed_data['humidity'] = value['value']
        if processed_data['pm1'] != 'NA':  # Check if default values have changed
            for key in processed_data:
                if key == 'pressure':
                    processed_data[key] = str(round(float(processed_data[key])/100))
                else:
                    processed_data[key] = str(round(float(processed_data[key])))
    return processed_data
