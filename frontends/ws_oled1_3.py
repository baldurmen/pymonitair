"""
Frontend glue code to drive the Waveshare Pico OLED 1.3 inch display using the
SH1107 driver
"""

from machine import SPI, Pin

from libs import SH1107


# Pin layout for the Waveshare Pico OLED 1.3
DC = 8
RST = 12
MOSI = 11
SCK = 10
CS = 9


def init_oled():
    """Initialise the WS OLED and return objects"""
    key0 = Pin(15, Pin.IN, Pin.PULL_UP)
    key1 = Pin(17, Pin.IN, Pin.PULL_UP)
    spi1 = SPI(1,
               baudrate=20000_000,
               polarity=0,
               phase=0,
               sck=Pin(SCK),
               mosi=Pin(MOSI),
               miso=None)
    display = SH1107.SH1107_SPI(width=128,
                                height=64,
                                spi=spi1,
                                dc=Pin(DC),
                                cs=Pin(CS),
                                res=Pin(RST))
    return display, key0, key1


def print_card(display, card_name):
    """Print identification card for the sensor value to be displayed"""
    if card_name == 'pm1':
        display.large_text(s="PM", x=-3, y=7, m=3)
        display.large_text(s="1", x=10, y=35, m=3)
    if card_name == 'pm2_5':
        display.large_text(s="PM", x=-3, y=7, m=3)
        display.large_text(s="2", x=-3, y=35, m=3)
        display.fill_rect(21, 54, 2, 2, 1)
        display.large_text(s="5", x=23, y=35, m=3)
    if card_name == 'pm10':
        display.large_text(s="PM", x=-3, y=7, m=3)
        display.large_text(s="10", x=-3, y=35, m=3)
    if card_name == 'humidity':
        display.large_text(s="HUMI", x=8, y=11, m=1)
        display.large_text(s="%", x=12, y=35, m=3)
    if card_name == 'temperature':
        display.large_text(s="TEMP", x=8, y=11, m=1)
        display.circle(x=10, y=35, radius=3, c=1)
        display.large_text(s="C", x=15, y=35, m=3)
    if card_name == 'pressure':
        display.large_text(s="PRESS", x=6, y=11, m=1)
        display.large_text(s="hPA", x=2, y=35, m=2)
    display.vline(55, 0, 64, 1)


def print_alert(display, state):
    """Print the alert rectangle"""
    if state is True:
        color = 1
    else:
        color = 0
    display.fill_rect(56, 0, 73, 64, color)
    display.show()


def print_sensor(display, sensor_value):
    """Print the sensor value at the appropriate place and size"""
    if len(sensor_value) == 1:
        x_value = 75
        y_value = 15
        m_value = 5
    elif len(sensor_value) == 2:
        x_value = 62
        y_value = 17
        m_value = 4
    elif len(sensor_value) == 3:
        x_value = 58
        y_value = 22
        m_value = 3
    elif len(sensor_value) == 4:
        x_value = 62
        y_value = 25
        m_value = 2
    display.large_text(s=sensor_value, x=x_value, y=y_value, m=m_value)


def print_error(display):
    """Print error message"""
    display.fill(0)
    display.text("Cannot connect", 0, 0)
    display.text("to given URL", 0, 15)
    display.show()
