pymonitair is a MicroPython project that aims to display weather data from a
home weather station (like the ones sold by [AirGradient][]) on a display.

This code was written for the Raspberry Pi Pico W, the [Waveshare Pico OLED
1.3][waveshare] screen and [RevolvAir Revo 1][revo1] weather station, but can
be adapted to other displays and stations by adding files in the `frontends`
and `backends` directories. The general MicroPython code isn't specific to the
Raspberry Pi Pico and shouldn't need to be modified.

pymonitair features:

* 6 different pages for the supported weather data (PM1, PM2.5, PM10,
  Temperature, Humidity and Pressure), accessible via the `key0` button
* Alerting (screen flashes) on the PM2.5 page when the defined threshold has
  been crossed
* Automatic screen off after a defined amount of time to save the OLED screen
  from burn-in
* Manual screen off using the `key1` button

![Demo video](demo.webm)

# Installation

Copy all the Python files to your board while keeping the directory tree, using
something like `thonny`.

You'll then need to configure the device by changing the values in the
`conf.py` file to match your device and network.

# License

The code I wrote is licensed under the GPLv3+. For the embedded libraries, see
the COPYRIGHTS file.

Many thanks to [Peter Lumb][peter] for the [OLED driver][sh1107] and the
[improved frambuffer][framebuf2] code.

[airgradient]: https://www.airgradient.com/
[waveshare]: https://www.waveshare.com/wiki/Pico-OLED-1.3
[revo1]: https://revolvair.org/revo1-station-danalyse-de-particules-fines-boutique/

[peter]: https://github.com/peter-l5/SH1107
[framebuf2]: https://github.com/peter-l5/framebuf2
[SH1107]: https://github.com/peter-l5/SH1107
