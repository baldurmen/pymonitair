"""
Nested loops to run the main logic code
"""

import time

from machine import WDT

import conf

from networking import fetch_data
from frontends.ws_oled1_3 import print_card, print_sensor, print_alert
from backends.revolvair import json_to_dict


class NestedLoops():
    """Nested loops"""
    sensors = ['pm1', 'pm2_5', 'pm10', 'humidity', 'temperature', 'pressure']
    refresh_rate = conf.refresh_rate
    timeout = conf.timeout
    pm2_5_critical = conf.pm2_5_critical

    def __init__(self, display, key0, key1):
        self.display = display
        self.key0 = key0
        self.key1 = key1
        self.counter = 1  # default to PM2.5 page
        self.data = {}
        self.sleep = {'value': False, 'manual': False, 'counter': 0}
        self.main_time = self.refresh_time = self.page_time = 0
        self.wdt = WDT(timeout=8300)
        self.wdt.feed()

    def debounce(self):
        """Debounce key press"""
        time.sleep_ms(80)

    def screen_off(self, cycle):
        """Poweroff the screen after a number of cycles when there's no alert"""
        self.sleep['counter'] += 1
        if ((self.sleep['counter'] == cycle) and
            (int(self.data['pm2_5']) < self.pm2_5_critical)):
            self.sleep['counter'] = 0
            self.sleep['value'] = True
            self.display.sleep(True)

    def alert(self):
        """
        Flash the screen when the PM2.5 value is above the critical threshold
        """
        if self.data['pm2_5'] != 'NA':
            if int(self.data['pm2_5']) > self.pm2_5_critical:
                # Wake the screen if it was turned off by the screen timeout
                if (self.sleep['value'] is True) and (self.sleep['manual'] is False):
                    self.display.sleep(False)
                time.sleep(1)
                print_alert(self.display, True)
                time.sleep(1)
                print_alert(self.display, False)

    def print_page(self):
        """Print sensor page"""
        display = self.display
        sensors = self.sensors
        counter = self.counter
        display.fill(0)
        print_card(display, sensors[counter])
        print_sensor(display, self.data[sensors[counter]])
        display.show()

    def main_loop(self):
        """Main loop"""
        self.wdt.feed()
        while True:
            json_data = fetch_data(self.display)
            self.data = json_to_dict(json_data)
            self.main_time = time.time()
            self.refresh_time = time.time()
            self.refresh_loop()
            self.screen_off(self.timeout)

    def refresh_loop(self):
        """Loop to refresh data each refresh_rate"""
        while self.main_time + self.refresh_rate > self.refresh_time:
            self.refresh_time = time.time()
            self.page_time = time.time()
            if self.counter == len(self.sensors):
                self.counter = 0
            while ((self.counter <= len(self.sensors) - 1) and
                   (self.refresh_time + 5 > self.page_time)):
                self.page_loop()
                self.wdt.feed()

    def page_loop(self):
        """Loop through sensor pages"""
        self.page_time = time.time()
        self.print_page()
        if self.key0.value() == 0:
            self.counter += 1
            self.debounce()
        if self.key1.value() == 0:
            self.sleep['value'] = not self.sleep['value']
            self.sleep['manual'] = not self.sleep['manual']
            self.display.sleep(self.sleep['value'])
            self.debounce()
        if self.counter == 1 and self.pm2_5_critical is not None:
            self.alert()
